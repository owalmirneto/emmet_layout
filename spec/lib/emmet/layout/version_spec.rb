# frozen_string_literal: true
require 'spec_helper'

module Emmet
  describe Layout do
    it 'has a version number' do
      expect(Layout::VERSION).not_to be_nil
    end
  end
end
