# frozen_string_literal: true
Rails.application.routes.draw do
  # mount Emmet::Layout::Engine => '/emmet_layout'
  root 'home#index'
end
