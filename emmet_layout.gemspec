# frozen_string_literal: true
$LOAD_PATH.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'emmet/layout/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'emmet_layout'
  s.version     = Emmet::Layout::VERSION
  s.authors     = ['Walmir Neto']
  s.email       = ['wfsneto@gmail.com']
  s.homepage    = 'https://bitbucket.org/wfsneto/emmet_layout'
  s.summary     = 'Emmet Layout'
  s.description = 'Layout for apps Emmet.'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'rails', '~> 4.2', '>= 4.2.6'
  s.add_dependency 'less-rails', '~> 2.7', '>= 2.7.1'
  s.add_dependency 'therubyracer', '~> 0.12.2'

  s.add_development_dependency 'sqlite3', '~> 1.3', '>= 1.3.11'
  s.add_development_dependency 'rubocop', '~> 0.38.0'
  s.add_development_dependency 'factory_girl_rails', '~> 4.6'
  s.add_development_dependency 'rspec-rails', '~> 3.4', '>= 3.4.2'
  s.add_development_dependency 'shoulda-matchers', '~> 3.1', '>= 3.1.1'
end
