# frozen_string_literal: true
module Emmet
  module Layout
    class ApplicationMailer < ActionMailer::Base
      default from: 'from@example.com'
      layout 'mailer'
    end
  end
end
