# frozen_string_literal: true
module Emmet
  module Layout
    class ApplicationController < ActionController::Base
      protect_from_forgery with: :exception
    end
  end
end
