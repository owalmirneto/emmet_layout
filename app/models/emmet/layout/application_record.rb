# frozen_string_literal: true
module Emmet
  module Layout
    class ApplicationRecord < ActiveRecord::Base
      self.abstract_class = true
    end
  end
end
