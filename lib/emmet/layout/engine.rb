# frozen_string_literal: true
module Emmet
  module Layout
    class Engine < ::Rails::Engine
      isolate_namespace Emmet::Layout
    end
  end
end
